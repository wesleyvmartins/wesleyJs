### Olá! Eu sou o Wesley Viana

- 🔭 Desenvolvedor Fullstack Web na DFranquias
- 📖 Cursando <strong>Analise e Desenvolvimento de Sistemas</strong> pela Estácio
- 🌱 Estudante Front End & Back End 
- 📫 Contate-me no email: wesley.vmartins.js@gmail.com
 
 <div class="initial">
  <a href="https://github.com/wesleyJs">
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=wesleyJs&show_icons=true&theme=false&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=wesleyJs&layout=compact&langs_count=7&theme=false"/>
</div>
<div style="display: inline_block"><br>
  <img align="center" alt="Rafa-Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img align="center" alt="Rafa-HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="Rafa-CSS" height="30" width="40" 
src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
</div>
<br>
<hr>
 <div style="text-align: center"> 
  <a href="https://www.instagram.com/wesley_vmartins/" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/wesley-martins-103430207/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>  
</div>
